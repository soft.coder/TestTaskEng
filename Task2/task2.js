
window.onload = function() {
    const COUNT_MOMENT_NOMINAL = 3;
    const MAX_NOMINAL = 100;
    const MIN_NOMINAL = 1;
    const MAX_COST = 1000;
    const MIN_COST = 1;
    var cost = MIN_COST + Math.floor(Math.random() * MAX_COST);
    console.log("COST:", cost);
    var monetNominal;

    function generateNominal() {
        monetNominal = [];
        function generate() {
            return MIN_NOMINAL + Math.floor(Math.random() * MAX_NOMINAL)
        }

        monetNominal.push(generate());
        for (var i = 1; i < COUNT_MOMENT_NOMINAL; i++) {
            do {
                var nominal = generate();
            } while (monetNominal.indexOf(nominal) != -1);
            monetNominal.push(nominal);
        }
        monetNominal = monetNominal.sort(function(a, b){
            return b - a;
        });
        console.log("monetNominal", monetNominal);
    }

    generateNominal();
    var monetChoose = [];


    function isNeedDeliverRecursive(sum, monetChooseRecursive){
        //console.log("recursive: ", monetChooseRecursive);
        if(sum === cost){
            //console.log("success", monetChooseRecursive);
            monetChoose = monetChooseRecursive;
            return true;
        }
        else if(sum < cost) {
            for (var i=0; i < monetNominal.length; i++) {
                if (isNeedDeliverRecursive(sum + monetNominal[i], monetChooseRecursive.concat(monetNominal[i]))) return true;
            }
        }
        //console.log("fail: ", monetChooseRecursive);
        return false;
    }
    var result = isNeedDeliverRecursive(0, []);

    console.log(result ? "success:" : "fail:", monetChoose);
    var sumCheck = 0;
    for(var i=0; i < monetChoose.length; i++) sumCheck += monetChoose[i];
    console.log("check: ", sumCheck === cost, sumCheck);
};