function GetTranslate(string, onsuccess, onerror){
    const URL = "https://dictionary.skyeng.ru/api/v2/search-word-translation?text=";
    string = encodeURIComponent(string).replace(/%20/g, "+");

    onsuccess = onsuccess || function(){};
    onerror = onerror || function(){};

    var request = new XMLHttpRequest();

    request.open("GET", URL + string);
    request.onreadystatechange = function(){
        if(request.readyState === 4 && request.status === 200){
            if(request.status === 200) {
                var response = JSON.parse(request.responseText);
                if (response.length > 0)
                    onsuccess(response[0]);
                else
                    onsuccess();
            }
            else{
                onerror(request.status, request.statusText);
            }
        }
    };
    request.send(null);
}

window.onload = function(){
    var cart = new Cart(document.querySelector(".translate-cart"));
    var prevSelectionText = "";
    document.body.addEventListener('mouseup', function(event) {
        var selectionText = window.getSelection().toString().trim();
        if(selectionText && prevSelectionText != selectionText){
            prevSelectionText = selectionText;
            GetTranslate(selectionText, function(translate){
                cart.init(event.pageX, event.pageY, translate);
            });
        }
    }, false);
};

function Word(domElement){
    this._domElement = domElement;
}
Word.prototype.setText = function(text){
    this._domElement.innerHTML = text;
};

function SourceWord(cartDomElement){
    Word.call(this, cartDomElement.querySelector(".original-word"));
}
SourceWord.prototype = new Word();
SourceWord.prototype.constructor = SourceWord;

function Meaning(domElement){
    Word.call(this, domElement);
}
Meaning.prototype = new Word();
Meaning.prototype.constructor = Meaning;

function MeaningFirst(cartDomElement){
    Meaning.call(this, cartDomElement.querySelector(".first-meaning"));
}
MeaningFirst.prototype = new Meaning();
MeaningFirst.prototype.constructor = MeaningFirst;


function OtherMeaning(index, translation){
    var meaning = document.createElement("p");
    meaning.className = "meaning";
    //meaning.innerHTML = translation + '<b class="meaning-bold">|</b>';
    this.onhover = function(){};
    this.onleave = function(){};
    var self = this;
    meaning.addEventListener("mouseover", function(event){
        event.target.style.fontWeight = "bold";
        self.onhover(index);
    },false);
    meaning.addEventListener("mouseleave", function(event){
        event.target.style.fontWeight = "normal";
        self.onleave(index);
    },false);
    this.getHtmlElement = function(){
        return meaning;
    };
    Meaning.call(this, meaning);
    this.setText(translation + '<b class="meaning-bold">|</b>');
}

OtherMeaning.prototype = new Meaning();
OtherMeaning.prototype.constructor = OtherMeaning;

function MeaningOtherCollection(cartDomElement){
    this._domElement = cartDomElement.querySelector(".other-meanings");
    this._domElement.innerHTML = "";
}

MeaningOtherCollection.prototype.init = function(meanings, onhover, onleave){
    this._domElement.innerHTML = "";
    for(var i=1; i < meanings.length; i++){
        var meaning = new OtherMeaning(i, meanings[i].translation);
        meaning.onhover = onhover;
        meaning.onleave = onleave;
        this._domElement.appendChild(meaning.getHtmlElement());
    }
};

function MeaningImage(url){
    var image = new Image();
    var isLoad = false;
    this.onload = function(){};
    var self = this;
    this.load = function(){
        if(isLoad) {
            self.onload();
            return;
        }
        var img = new Image();
        img.onload = function(){
            image.src = img.src;
            isLoad = true;
            self.onload();
        };
        img.src = "https:" + url;
    };
    this.getUrl = function(){
        return url;
    }
}

function MeaningImageCollection(cartDomElement){
    this.meaningImg = cartDomElement.querySelector(".meaning-img");
    this.meaningImages = [];
    var self = this;

    this.switchMeaningImage = function(index, onComplete){
        switchImage(index, onComplete);
    };
    this.resetMeaningImage = function(){
        switchImage(0);
    };
    function switchImage(index, onComplete){
        onComplete = onComplete || function(){};
        self.meaningImages[index].onload = function(){
            self.meaningImg.src = self.meaningImages[index].getUrl();
            onComplete();
        };
        self.meaningImages[index].load();
    }
}
MeaningImageCollection.prototype.init = function(meanings, onloadfirstimage){
    this.meaningImages = [];
    this.meaningImages.push(new MeaningImage(meanings[0].image_url));
    var self = this;
    this.meaningImages[0].onload = function(){
        self.meaningImg.onload = function() {
            onloadfirstimage();
        };
        self.meaningImg.src = self.meaningImages[0].getUrl();
        for(var i=1; i < meanings.length; i++){ // for browser cache
            self.meaningImages[i] = new MeaningImage(meanings[i].image_url);
            self.meaningImages[i].load();
        }
    };
    this.meaningImages[0].load();
};


function Cart(domElement){
    this.x = 0;
    this.y = 0;
    this.translateCart = domElement;
    this.sourceWord = new SourceWord(domElement);
    this.meaningFirst = new MeaningFirst(domElement);
    this.meaningOtherCollection = new MeaningOtherCollection(domElement);
    this.meaningImageCollectoin = new MeaningImageCollection(domElement);

}
Cart.prototype.move = function(x, y){
    this.x = x || 0; this.y = y || 0;
    if(this.x + this.translateCart.offsetWidth < document.body.clientWidth) {
        this.translateCart.style.left = (this.x + 10) + "px";
    }
    else{
        this.translateCart.style.left = (this.x - this.translateCart.offsetWidth - 10) + "px";
    }

    if(this.y + this.translateCart.offsetHeight < window.innerHeight || (this.y - this.translateCart.offsetHeight -  25) < 0){
        this.translateCart.style.top = (this.y + 25) + "px";
    }
    else{
        this.translateCart.style.top = (this.y - this.translateCart.offsetHeight - 25) + "px";
    }
}
Cart.prototype.init = function(x, y, translate){
    var self = this;
    if(translate && translate.meanings && translate.meanings.length > 0){
        this.meaningImageCollectoin.init(translate.meanings, function(){ self.move(x, y); self.show()});
        console.log("source", translate.text);
        this.sourceWord.setText(translate.text);
        console.log("source", this.sourceWord._domElement.innerHTML);
        this.meaningFirst.setText(translate.meanings[0].translation);
        this.meaningOtherCollection.init(translate.meanings, this.meaningImageCollectoin.switchMeaningImage, this.meaningImageCollectoin.resetMeaningImage);
    }
};

Cart.prototype.show = function(){
    this.translateCart.style.visibility = "visible";
    var self = this;
    window.addEventListener("mouseup", function(event){
        function isChildTranslateCart(domElement){
            if(domElement === self.translateCart)
                return true;
            else {
                if (domElement.parentNode)
                    return isChildTranslateCart(domElement.parentNode);
                else
                    return false;
            }
        }
        if(!isChildTranslateCart(event.target)){
            self.hide();
        }
    }, true);
};
Cart.prototype.hide = function(){
    this.translateCart.style.visibility = "hidden";
};